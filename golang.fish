set --global --export GOPATH $HOME/.go

if not test -d $GOPATH
  mkdir $GOPATH
end

if not contains $GOPATH/bin $PATH
  set --append PATH $GOPATH/bin
end